import re
from collections import Counter
import numpy as np
import pandas as pd

def process_data(file_name):
    words = []     with open('shakespeare.txt') as f:
        read_data = f.read()
    pro_text = read_data.lower()
    words = re.findall(r'\w+', pro_text)
    return words

word_l = process_data('shakespeare.txt')
vocab = set(word_l)  # this will be your new vocabulary
print(f"The first ten words in the text are: \n{word_l[0:10]}")
print(f"There are {len(vocab)} unique words in the vocabulary.")

def get_count(word_l):
    word_count_dict = {} 
    word_count_dict = Counter(word_l)
    return word_count_dict

word_count_dict = get_count(word_l)
print(f"There are {len(word_count_dict)} key values pairs")
print(f"The count for the word 'thee' is {word_count_dict.get('thee',0)}")

def get_probs(word_count_dict):
    probs = {}  dict_values= word_count_dict.values()
    total = sum(dict_values)
    #print(total)
    for k in word_count_dict.keys():
        probs[k] = word_count_dict.get(k,0) / total
    return probs

probs = get_probs(word_count_dict)
print(f"Length of probs is {len(probs)}")
print(f"P('thee') is {probs['thee']:.4f}")

def delete_letter(word, verbose=False):
    delete_l = []
    split_l = []
    split_l = [(word[:i] , word[i:]) for i in range(len(word)) ]
    delete_l = [L + R[1:] for L , R in split_l if R ]
    if verbose: print(f"input word {word}, \nsplit_l = {split_l}, \ndelete_l = {delete_l}")
    return delete_l

delete_word_l = delete_letter(word="cans",verbose=True)

print(f"Number of outputs of delete_letter('at') is {len(delete_letter('at'))}")

def switch_letter(word, verbose=False):
    switche_l = []
    switch_l = []
    split_l = []
    item_l = []
    
    split_l = [(word[:i] , word[i:]) for i in range(len(word)) ]
    switche_l = [R[1] + R[0] + R[2:] for L , R in split_l if  len(L) == 0 and len(R) > 1  ]
    item1_l = [R[0] + L[0] + R[1:] for L , R in split_l if R and len(L) == 1 ]
    item2_l = [L[:-1] + R[0] + L[len(L)-1] + R[1:] for L , R in split_l if R and len(L) > 1  ]
    switche_l.extend(item1_l)
    switche_l.extend(item2_l)
    switches_l = set(switche_l)
    switch_l = list(switches_l)
    
    if verbose: print(f"Input word = {word} \nsplit_l = {split_l} \nswitch_l = {switch_l}") 
    return switch_l

switch_word_l = switch_letter(word="eta",verbose=True)

print(f"Number of outputs of switch_letter('at') is {len(switch_letter('at'))}")

def replace_letter(word, verbose=False):
    
    letters = 'abcdefghijklmnopqrstuvwxyz'
    replaces_l = []
    split_l = []
    replace_l = []
    
    split_l = [(word[:i] , word[i:]) for i in range(len(word)) ]
    for L, R in split_l:
        if(len(L) > 1):
            for j in range (1 , len(L) ):
                for i in range ( 0 , 26 ):
                    if(L[len(L) - 1] != letters[i]) : 
                        replaces_l.append(L[:-1]+ letters[i] + R)
        if(len (L) == 1):
            for i in range ( 0 , 26 ):
                if(L[len(L)-1] != letters[i]) :                    
                    replaces_l.append(letters[i] + R)
        if(len (R) == 1):
            for i in range ( 0 , 26 ):
                if(R[0] != letters[i]) :                    
                    replaces_l.append(L + letters[i] )
                    
    replace_set = set(replaces_l)
    
    # turn the set back into a list and sort it, for easier viewing
    replace_l = sorted(list(replace_set))
    
    if verbose: print(f"Input word = {word} \nsplit_l = {split_l} \nreplace_l {replace_l}")   
    return replace_l

replace_l = replace_letter(word='can',verbose=True)

print(f"Number of outputs of switch_letter('at') is {len(replace_letter('at'))}")

def insert_letter(word, verbose=False):
    
    letters = 'abcdefghijklmnopqrstuvwxyz'
    insert_l = []
    split_l = []
    
    split_l = [(word[:i] , word[i:]) for i in range(len(word) +1) ] 
    for L , R in split_l :
            for i in range ( 0 , 26 ):                    
                    insert_l.append(L + letters[i] + R)

    if verbose: print(f"Input word {word} \nsplit_l = {split_l} \ninsert_l = {insert_l}")
    return insert_l

insert_l = insert_letter('at', True)
print(f"Number of strings output by insert_letter('at') is {len(insert_l)}")

print(f"Number of outputs of insert_letter('at') is {len(insert_letter('at'))}")

def edit_one_letter(word, allow_switches = True):
    edit_one_set = set()
    
    set1 = set(delete_letter(word))
    set2 = set(replace_letter(word))
    set3 = set(insert_letter(word))
    if allow_switches : 
    	set4 = set(switch_letter(word))
    	set3.update(set4)
    set2.update(set3)
    set1.update(set2)
    edit_one_set = set1

    return edit_one_set

tmp_word = "at"
tmp_edit_one_set = edit_one_letter(tmp_word)
# turn this into a list to sort it, in order to view it
tmp_edit_one_l = sorted(list(tmp_edit_one_set))

print(f"input word {tmp_word} \nedit_one_l \n{tmp_edit_one_l}\n")
print(f"The type of the returned object should be a set {type(tmp_edit_one_set)}")
print(f"Number of outputs from edit_one_letter('at') is {len(edit_one_letter('at'))}")

def edit_two_letters(word, allow_switches = True):
    
    edit_two_set = set()
    edit_two_set = set()
    
    edit_one_set = edit_one_letter(word)
    for w in edit_one_set:
        edit_two_set.update(edit_one_letter(w))
    
    return edit_two_set

tmp_edit_two_set = edit_two_letters("a")
tmp_edit_two_l = sorted(list(tmp_edit_two_set))
print(f"Number of strings with edit distance of two: {len(tmp_edit_two_l)}")
print(f"First 10 strings {tmp_edit_two_l[:10]}")
print(f"Last 10 strings {tmp_edit_two_l[-10:]}")
print(f"The data type of the returned object should be a set {type(tmp_edit_two_set)}")
print(f"Number of strings that are 2 edit distances from 'at' is {len(edit_two_letters('at'))}")

def get_corrections(word, probs, vocab, n=2, verbose = False):
    
    suggestions = []
    n_best = []
    best_words = {}
    #suggestion = {}
    
    if word in vocab : 
        suggestions.append(word)
    if not suggestions :
        for w in edit_one_letter(word) :
            if w in vocab : 
                suggestions.append(w)
    if not suggestions :
        for w in edit_two_letters(word) :
            if w in vocab : 
                suggestions.append(w)
    if not suggestions : 
        suggestions.append(word)
    
    for w in suggestions :
        best_words[w] = probs[w]
    
    n_best_l = Counter(best_words)
    n_best = n_best_l.most_common(n)
    
    if verbose: print("suggestions = ", suggestions)
    return n_best

#Test
my_word = 'dys' 
tmp_corrections = get_corrections(my_word, probs, vocab, 2, verbose=True)
for i, word_prob in enumerate(tmp_corrections):
    print(f"word {i}: {word_prob[0]}, probability {word_prob[1]:.6f}")

print(f"data type of corrections {type(tmp_corrections)}")

def min_edit_distance(source, target, ins_cost = 1, del_cost = 1, rep_cost = 2):
    
    m = len(source) 
    n = len(target) 
    #initialize cost matrix with zeros and dimensions (m+1,n+1) 
    D = np.zeros((m+1, n+1), dtype=int) 
    
    for row in range(1,m+1): # Replace None with the proper range
        D[row,0] = D[row-1,0]+1
        
    for col in range(1,n+1): # Replace None with the proper range
        D[0,col] = D[0,col-1]+1
        
    for row in range(1,m+1): 
        
        for col in range(1,n+1):
            r_cost = rep_cost
            # Check to see if source character at the previous row
            # matches the target character at the previous column, 
            if source[row-1] == target[col-1]:
                # Update the replacement cost to 0 if source and target are the same
                r_cost = 0
                
            # Update the cost at row, col based on previous entries in the cost matrix
            D[row,col] = min(D[row-1,col] + 1 , D[row, col-1] + 1 , D[row-1 , col-1] + r_cost)
          
    # Set the minimum edit distance with the cost found at row m, column n
    med = D[m , n]
    
    return D, med

# test
source =  'play'
target = 'stay'
matrix, min_edits = min_edit_distance(source, target)
print("minimum edits: ",min_edits, "\n")
idx = list('#' + source)
cols = list('#' + target)
df = pd.DataFrame(matrix, index=idx, columns= cols)
print(df)

# testing your implementation 
source =  'eer'
target = 'near'
matrix, min_edits = min_edit_distance(source, target)
print("minimum edits: ",min_edits, "\n")
idx = list(source)
idx.insert(0, '#')
cols = list(target)
cols.insert(0, '#')
df = pd.DataFrame(matrix, index=idx, columns= cols)
print(df)

#edit_one_letter
source = "eer"
targets = edit_one_letter(source,allow_switches = False)  
for t in targets:
    _, min_edits = min_edit_distance(source, t,1,1,1)  # set ins, del, sub costs all to one
    if min_edits != 1: print(source, t, min_edits)

#edit_two_letters
source = "eer"
targets = edit_two_letters(source,allow_switches = False) #disable switches since min_edit_distance does not include them
for t in targets:
    _, min_edits = min_edit_distance(source, t,1,1,1)  # set ins, del, sub costs all to one
    if min_edits != 2 and min_edits != 1: print(source, t, min_edits)



